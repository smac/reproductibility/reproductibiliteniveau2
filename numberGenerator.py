import random

def setSeed(seed) -> None:
    random.seed(seed)

def generateNumbers(tries,min=0,max=10) -> list[int]:
    numbers = []
    for _ in range (tries) :
        numbers.append(random.randint(min,max))
    return numbers