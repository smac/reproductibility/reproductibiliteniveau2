# ReproductibiliteNiveau2

Exemple de projet reproductible

Dans cette version, on ajoute une gestion de l'aléatoire et du paramétrage et le workflow est formalisé avec un gestion des tags. De cette façon l'expérimentateur peut plus facilement relancer avec les même paramètres et obtenir le même résultat