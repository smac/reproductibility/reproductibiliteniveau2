import matplotlib.pyplot as plt
import numpy as np

def computeValues(values,min=0,max=10) -> None:
    plt.hist(values,range=(min,max),bins=max+1,color='blue',edgecolor='black')
    print("figure saved in 'output/histogram.png'")
    plt.savefig("outputs/histogram.png")
    plt.show()
    return float(np.average(values))